package ru.ylevin;

import java.util.Random;

public class App {

    public static void main(String[] args) {
        double[] array = new double[100000];
        Random r = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = r.nextDouble() * 10;
        }

        System.out.println(array[0]);
        System.out.println(array[1]);
        System.out.println(array[2]);
        System.out.println(array[array.length - 1]);
        System.out.println(array[array.length - 2]);

        ArrayPowExecutor s = new SimpleArrayPowExecutor();
        ArrayPowExecutor f = new DistributedArrayPowExecutor(1000);
        ArrayPowExecutor p = new StreamArrayPowExecutor();
        ArrayPowExecutor p2 = new StreamArrayPowExecutor2(1000);


        test(s, array, r);
        test(p, array, r);
        test(f, array, r);

        int fTime = test(f, array, r);
        int sTime = test(s, array, r);
        int pTime = test(p, array, r);
        int p2Time = test(p2, array, r);

        System.out.println(s.getClass() + ": " + sTime);
        System.out.println(f.getClass() + ": " + fTime);
        System.out.println(p.getClass() + ": " + pTime);
        System.out.println(p2.getClass() + ": " + p2Time);
    }

    static int test(ArrayPowExecutor e, double[] array, Random r) {
        long  start = System.currentTimeMillis();
        for (int i = 0; i < 200; i++) {
            double[] result = e.pow(array, r.nextDouble() * 10);
            System.out.println(result[0]);
            System.out.println(result[1]);
            System.out.println(result[2]);
            System.out.println(result[array.length - 1]);
            System.out.println(result[array.length - 2]);
        }
        return (int) (System.currentTimeMillis() - start);
    }
}
