package ru.ylevin;

import java.util.stream.IntStream;

/**
 * Created by ylevin on 7/6/14.
 */
public class StreamArrayPowExecutor implements ArrayPowExecutor {
    @Override
    public double[] pow(final double[] a, final double b) {
        final double[] result = new double[a.length];
        IntStream.range(0, a.length).parallel().forEach(i -> {
            result[i] = Math.pow(a[i], b);
        });

        return result;
    }
}
