package ru.ylevin;

import java.util.stream.IntStream;

/**
 * Created by ylevin on 7/6/14.
 */
public class StreamArrayPowExecutor2 implements ArrayPowExecutor {

    final int blockSize;

    public StreamArrayPowExecutor2(int blockSize) {
        this.blockSize = blockSize;
    }


    @Override
    public double[] pow(final double[] a, final double b) {
        final double[] result = new double[a.length];
        IntStream.range(0, a.length / blockSize).parallel().forEach(i -> {
            int offset = blockSize * i;
            int limit = Math.min(offset + blockSize, a.length);
            for (int  j = offset; j < limit; j++) {
                result[j] = Math.pow(a[j], b);
            }
        });

        return result;
    }
}
