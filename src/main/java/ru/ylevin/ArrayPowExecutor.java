package ru.ylevin;

/**
 * Created by ylevin on 7/5/14.
 */
public interface ArrayPowExecutor {
    double[] pow(double[] a, double b);
}
